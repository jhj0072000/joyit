<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Item;


 /**
 * @SWG\Get(
 *   path="/item",
 *   summary="List Items",
 *   operationId="getItemLists",
 *   @SWG\Response(response=200, description="successful operation"),
 *   @SWG\Response(response=406, description="not acceptable"),
 *   @SWG\Response(response=500, description="internal server error")
 * )
 *
 */

 /**
 * @SWG\Get(
 *   path="/item/{itemId}",
 *   summary="Get an item by id",
 *   operationId="getItemById",
 *   @SWG\Parameter(
 *     name="itemId",
 *     in="path",
 *     description="Target item.",
 *     required=true,
 *     type="integer"
 *   ),
 *   @SWG\Response(response=200, description="successful operation"),
 *   @SWG\Response(response=406, description="not acceptable"),
 *   @SWG\Response(response=500, description="internal server error")
 * )
 *
 */

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Item::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $thisItem = Item::withCount('tickets')->find($id);
        $thisItem->load(['images' =>
                            function($query){
                                $query->where('image_type_id', 1)->where('image_ratio_id', 1);
                            }]);
        $relatedItems = Item::with(['images' =>
                            function($query){
                                $query->where('image_type_id', 1)->where('image_ratio_id', 1);
                            }])->withCount('tickets')
                            ->where('category_id', $thisItem->category_id)
                            ->where('id', '!=', $thisItem->id)
                            ->orderBy('tickets_count', 'desc')
                            ->get();
        $user = \Auth::user();
        return view('item')->with('thisItem', $thisItem)->with('user', $user)->with('relatedItems', $relatedItems);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Show items order by number of tickets
     *
     * @return \Illuminate\Http\Response
     */

    public function showTop50()
    {
        $top50Items = \DB::table('items')
                        ->leftJoin('tickets', 'tickets.item_id', '=', 'items.id')
                        ->select('items.*',\DB::raw('count(tickets.id) AS ticketCount'))
                        ->groupBy('items.id')
                        ->orderBy('ticketCount', 'desc')
                        ->take(50)
                        ->get();

        return view('top50')->with('items', $top50Items);
    }

    /**
    * Select raffle winner and return winner id
    * @param int $id
    * @return int
    */
    public function selectWinner($item_id)
    {
        $item = Item::find($item_id);
        $tickets = $item->tickets()->get();
        if($tickets->count() != $item->price)
        {
            return response('Item must be full with tickets', 412)
                  ->header('Content-Type', 'text/plain');
        }
        else if($tickets->where('won', 1)->count() > 0)
        {
            return response('This item already have winner', 412)
                  ->header('Content-Type', 'text/plain');
        }
        else
        {
            $winningTicket = $tickets->random(1);
            $winningTicket->won = 1;
            $winningTicket->save();
            return $winningTicket;
        }
    }
}
