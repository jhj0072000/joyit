<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use App\Item;
use App\AddressInfo;
use App\ImageType;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'fullName' => 'required',
            // 'address1' => 'required',
            // 'city' => 'required',
            // 'state' => 'required',
            // 'zipcode' => 'required'
        ]);

        $thisUser = \Auth::user();
        $address = $thisUser->addressInfos()->first();
        if(!isset($address))
            $address = new AddressInfo;

        $address->address1 = $request->address1;
        $address->address2 = $request->address2;
        $address->city = $request->city;
        $address->state = $request->state;
        $address->zipcode = $request->zipcode;
        $thisUser->addressInfos()->save($address);

        $thisUser->name = $request->fullName;
        $thisUser->save();

        return redirect('myaccount');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Show My Account Page.
     *
     * @return \Illuminate\Http\Response
     */
    public function showMyAccount()
    {
        if (!\Auth::check()) 
        {
            abort(403, 'Unauthorized action.');
        }

        $thisUser = \Auth::user();
        $thisUser->load('addressInfos');

        $ticketsUsed = $thisUser->ticketsUsed()->select('item_id')->get();
        $items = $items = Item::with(['tickets','images' => 
                                    function($query){
                                        $query->where('image_type_id', 1)->where('image_ratio_id', 1);
                                    }])->whereIn('id', $ticketsUsed)->get();

        return view('myaccount')->with('user', $thisUser)->with('items', $items);
    }
}
