<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Ticket;
use App\Item;


 /**
 * @SWG\Get(
 *   path="/ticket",
 *   summary="List tickets",
 *   operationId="getTicketLists",
 *   @SWG\Response(response=200, description="successful operation"),
 *   @SWG\Response(response=406, description="not acceptable"),
 *   @SWG\Response(response=500, description="internal server error")
 * )
 *
 */

 /**
 * @SWG\Get(
 *   path="/ticket/{ticketId}",
 *   summary="Get a ticket by id",
 *   operationId="getTicketById",
 *   @SWG\Parameter(
 *     name="ticketId",
 *     in="path",
 *     description="Target Ticket.",
 *     required=true,
 *     type="integer"
 *   ),
 *   @SWG\Response(response=200, description="successful operation"),
 *   @SWG\Response(response=406, description="not acceptable"),
 *   @SWG\Response(response=500, description="internal server error")
 * )
 *
 */

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Ticket::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Ticket::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
    *
    * @param int $id
    * @return \Illuminate\Http\Response
    */
    public function joyit(Request $request, $id)
    {
        if (!\Auth::check()) 
        {
            abort(403, 'Unauthorized action.');
        }
        $item = Item::findOrFail($id);
        $entryLeft = $item->price - $item->tickets()->count();
        $userTicketCount = \Auth::user()->getUnusedTicketCount();

        $maximumTicketCount = $userTicketCount;
        if($maximumTicketCount > $entryLeft)
        {
            $maximumTicketCount = $entryLeft;
        }

        $validator = \Validator::make($request->all(), [
            'ticket_count' => 'required|numeric|min:1|max:'.$maximumTicketCount,
        ]);
        if ($validator->fails()) {
            \Session::flash('message', "Ticket amount error");
            return \Redirect::back();
        }

        $requestedCount = $request->input('ticket_count');
        
        $tickets = \Auth::user()->getUnusedTicketListByCount($requestedCount);
        
        foreach($tickets as $ticket)
        {
            $ticket->item()->associate($item);
            $ticket->save();
        }

        return redirect()->action(
            'ItemController@show', ['id' => $item->id]
        );


    }

    
}
