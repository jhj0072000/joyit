<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\ItemType;
use App\Image;
use App\ImageType;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $joyitItemTypeId = ItemType::where('type', 'joyit')->first()->id;
        $items = Item::with(['images' => 
        function($query){
            $query->where('image_type_id', 1)->where('image_ratio_id', 1);
        }])->withCount('tickets')->where('item_type_id',$joyitItemTypeId)->get()->take(29);

        return view('home')->with('items', $items);
    }
}
