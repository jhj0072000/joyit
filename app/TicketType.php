<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketType extends Model
{
    protected $table = 'ticket_types';

    public function tickets()
   	{
   		return $this->hasMany('App\Ticket', 'ticket_type_id');
   	}
}
