<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCategoryClick extends Model
{
    protected $table = 'user_category_clicks';

    protected $fillable = [
        'count',
    ];

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function category()
    {
    	return $this->belongsTo('App\Category', 'category_id');
    }
}
