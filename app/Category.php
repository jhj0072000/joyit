<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    public function parent()
    {
    	return $this->belongsTo('App\Category', 'parent_id');
    }

    public function childs()
    {
    	return $this->hasMany('App\Category', 'parent_id');
    }

    public function items()
    {
    	return $this->hasMany('App\Item', 'category_id');
    }

    public function UserClick()
    {
    	return $this->hasMany('App\UserCategoryClick', 'category_id');
    }
}
