<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';

    public function item()
    {
    	return $this->belongsTo('App\Item', 'item_id');
    }

    public function imageType()
    {
    	return $this->belongsTo('App\ImageType', 'image_type_id');
    }

    public function imageRatio()
    {
    	return $this->belongsTo('App\ImageRatio', 'image_ratio_id');
    }
}
