<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Item;

class test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->selectWinner(1);
    }

    public function selectWinner($item_id)
    {
        $item = Item::find($item_id);
        $tickets = $item->tickets()->get();
        if($tickets->count() != $item->price)
        {
            echo $item->price;
            echo $tickets->count();
            echo 'need more ticket';
        }
        else
        {
            $winner = $tickets->random(1);
            echo $winner->id;
        }
    }
}
