<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table='items';

    public function category()
    {
    	return $this->belongsTo('App\Category', 'category_id');
    }

    public function itemType()
    {
    	return $this->belongsTo('App\ItemType', 'item_type_id');
    }

    public function images()
    {
    	return $this->hasMany('App\Image', 'item_id');
    }

    public function tickets()
    {
    	return $this->hasMany('App\Ticket', 'item_id');
    }

    public function getImageByTypeRatio($type, $ratio)
    {
        $imageTypeId = ImageType::where('type', $type)->first()->id;
        $imageRatioId = ImageRatio::where('ratio', $ratio)->first()->id;
        $image = $this->images()->where('image_type_id', $imageTypeId)->where('image_ratio_id', $imageRatioId)->get();
        return $image;
    }

    public function userWon($userId)
    {
        $count = $this->ticekts()->where('user_id', $userId)->where('won', 1)->count();
        if($count > 0)
            return True;
        else
            return False;
    }

    public function getFilledPercentage()
    {
        return number_format($this->hasMany('App\Ticket', 'item_id')->count()/$this->price*100, 2, '.', '');
    }
}
