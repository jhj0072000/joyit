<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'tickets';

    protected $fillable = [
        'price', 'won',
    ];

    public function item()
    {
    	return $this->belongsTo('App\Item', 'item_id');
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
