<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddressInfo extends Model
{
    protected $table = 'address_infos';

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
