<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageType extends Model
{
    protected $table = 'image_types';

    public function images()
   	{
   		return $this->hasMany('App\Image', 'image_type_id');
   	}
}
