<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function tickets()
    {
        return $this->hasMany('App\Ticket', 'user_id');
    }

    public function categoryClicks()
    {
        return $this->hasMany('App\UserCategoryClick', 'user_id');
    }

    public function addressInfos()
    {
        return $this->hasMany('App\AddressInfo', 'user_id');
    }

    public function getUnusedTicketCount()
    {
        return $this->tickets()->whereNull('item_id')->count();
    }

    public function getUnusedTicketListByCount($number)
    {
        return $this->tickets()->where('won', 0)->whereNull('item_id')->get()->take($number);
    }

    public function ticketsWon()
    {
        return $this->tickets()->where('won', 1)->get();
    }

    public function ticketsPlaying()
    {
        return $this->tickets()->where('won', 0)->whereNotNull('item_id')->get();
    }

    public function ticketsLost()
    {
        return $this->tickets()->where('won', 2)->get();
    }

    public function ticketsUsed()
    {
        return $this->tickets()->whereNotNull('item_id');
    }

}
