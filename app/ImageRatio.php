<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageRatio extends Model
{
    protected $table = 'image_ratios';

    public function images()
   	{
   		return $this->hasMany('App\Image', 'image_ratio_id');
   	}
}
