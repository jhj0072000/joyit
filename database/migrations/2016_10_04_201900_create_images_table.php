<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('path');
            $table->integer('item_id')->unsigned()->nullable();
            $table->integer('image_type_id')->unsigned();
            $table->integer('image_ratio_id')->unsigned();
            $table->timestamps();
            $table->foreign('image_type_id')->references('id')->on('image_types')->onUpdate('cascade');
            $table->foreign('image_ratio_id')->references('id')->on('image_ratios')->onUpdate('cascade');
            $table->foreign('item_id')->references('id')->on('items')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('images');
    }
}
