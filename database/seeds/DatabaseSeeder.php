<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(TicketTypesTableSeeder::class);
        $this->call(ItemTypesSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(ImageTypesTableSeeder::class);
        $this->call(ImageRatiosTableSeeder::class);
        $this->call(ItemsTableSeeder::class);
        $this->call(UserCategoryClicksTableSeeder::class);
        $this->call(TicketsTableSeeder::class);
    }
}
