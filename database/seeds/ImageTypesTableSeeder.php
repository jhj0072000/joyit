<?php

use Illuminate\Database\Seeder;
use App\ImageType;

class ImageTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $imageType = new ImageType;
        $imageType->type = 'main';
        $imageType->save();

        $imageType = new ImageType;
        $imageType->type = 'sub';
        $imageType->save();

        $imageType = new ImageType;
        $imageType->type = 'etc';
        $imageType->save();
    }
}
