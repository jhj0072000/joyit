<?php

use Illuminate\Database\Seeder;
use App\UserCategoryClick;
use App\User;
use App\Category;

class UserCategoryClicksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        foreach($users as $user)
        {
        	for($i = 0; $i < 100; $i++)
        	{
        		$thisCategory = Category::all()->random(1);
        		$userCategoryClick = UserCategoryClick::firstOrCreate(['user_id' => $user->id, 'category_id' => $thisCategory->id]);
        		$userCategoryClick->count += 1;
        		$userCategoryClick->save();
        	}
        }
    }
}
