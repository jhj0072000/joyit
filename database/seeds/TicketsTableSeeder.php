<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Ticket;

class TicketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        foreach($users as $user)
        {
        	for($i = 0; $i < 10; $i++)
        	{
        		$ticket = new Ticket;
        		$ticket->ticket_type_id = 1;
        		$ticket->won = 0;
                // $ticket->item_id = 1;
        		$user->tickets()->save($ticket);
        	}
        }

        $user = User::find(1);
        $ticket = new Ticket;
        $ticket->ticket_type_id = 1;
        $ticket->won = 1;
        $ticket->item_id = 1;
        $user->tickets()->save($ticket);

        $ticket = new Ticket;
        $ticket->ticket_type_id = 1;
        $ticket->won = 1;
        $ticket->item_id = 2;
        $user->tickets()->save($ticket);

        $ticket = new Ticket;
        $ticket->ticket_type_id = 1;
        $ticket->won = 1;
        $ticket->item_id = 3;
        $user->tickets()->save($ticket);

        // foreach($users as $user)
        // {
        //     for($i = 0; $i < 10; $i++)
        //     {
        //         $ticket = new Ticket;
        //         $ticket->ticket_type_id = 2;
        //         $ticket->won = 0;
        //         $user->tickets()->save($ticket);
        //     }
        // }

        // foreach($users as $user)
        // {
        //     for($i = 0; $i < 10; $i++)
        //     {
        //         $ticket = new Ticket;
        //         $ticket->ticket_type_id = 3;
        //         $ticket->won = 0;
        //         $user->tickets()->save($ticket);
        //     }
        // }
    }
}
