<?php

use Illuminate\Database\Seeder;

use App\TicketType;


class TicketTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ticketType = new TicketType;
        $ticketType->type = 'yellow';
        $ticketType->save();

        // $ticketType = new TicketType;
        // $ticketType->type = 'blue';
        // $ticketType->save();

        // $ticketType = new TicketType;
        // $ticketType->type = 'red';
        // $ticketType->save();
    }
}
