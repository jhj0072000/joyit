<?php

use Illuminate\Database\Seeder;
use App\Category;


class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = new Category;
        $category->name = 'Electronics';
        $category->save();

        $subCategory = new Category;
        $subCategory->name = 'TV';
        $category->childs()->save($subCategory);

        $subCategory = new Category;
        $subCategory->name = 'Computers';
        $category->childs()->save($subCategory);

        $subCategory = new Category;
        $subCategory->name = 'Audio/Video';
        $category->childs()->save($subCategory);

        $subCategory = new Category;
        $subCategory->name = 'Cameras';
        $category->childs()->save($subCategory);

        $subCategory = new Category;
        $subCategory->name = 'Mobile';
        $category->childs()->save($subCategory);

        $subCategory = new Category;
        $subCategory->name = 'Appliances';
        $category->childs()->save($subCategory);

        $subCategory = new Category;
        $subCategory->name = 'Video Games';
        $category->childs()->save($subCategory);



        $category = new Category;
        $category->name = 'Apparel';
        $category->save();

        $subCategory = new Category;
        $subCategory->name = 'Shoes';
        $category->childs()->save($subCategory);

        $subCategory = new Category;
        $subCategory->name = 'Clothes';
        $category->childs()->save($subCategory);



        $category = new Category;
        $category->name = 'Outdoors';
        $category->save();

        $subCategory = new Category;
        $subCategory->name = 'Camping';
        $category->childs()->save($subCategory);

        $subCategory = new Category;
        $subCategory->name = 'Hiking';
        $category->childs()->save($subCategory);



        $category = new Category;
        $category->name = 'Home';
        $category->save();



        $category = new Category;
        $category->name = 'Vehicles';
        $category->save();

        $subCategory = new Category;
        $subCategory->name = 'Cars';
        $category->childs()->save($subCategory);

        $subCategory = new Category;
        $subCategory->name = 'Motorcycles';
        $category->childs()->save($subCategory);
    }
}
