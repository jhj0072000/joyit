<?php

use Illuminate\Database\Seeder;
use App\ItemType;


class ItemTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $itemType = new ItemType;
        $itemType->type = 'merchandise';
        $itemType->save();

        $itemType = new ItemType;
        $itemType->type = 'services';
        $itemType->save();

        $itemType = new ItemType;
        $itemType->type = 'charity';
        $itemType->save();

        $itemType = new ItemType;
        $itemType->type = 'joyit';
        $itemType->save();
    }
}
