<?php

use Illuminate\Database\Seeder;
use App\Item;
use App\Category;
use App\ItemType;
use App\Image;
use App\ImageType;
use App\ImageRatio;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $item = new Item;
        $item->title = 'Mercedes Benz';
        $item->price = '70000.00';
        $item->description = 'Beautiful Mercedes Benz';

        $category = Category::where('name', 'Cars')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/benz.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Drone';
        $item->price = '500.00';
        $item->description = 'Beautiful Drone';

        $category = Category::where('name', 'Outdoors')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/drone.gif';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'BMW';
        $item->price = '70000.00';
        $item->description = 'Beautiful BMW';

        $category = Category::where('name', 'Cars')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/bmw.jpeg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Camera Bag';
        $item->price = '200.00';
        $item->description = 'Beautiful Camera Bag';

        $category = Category::where('name', 'Apparel')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/camera-bag.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Strong Cool';
        $item->price = '30.00';
        $item->description = 'Beautiful Strong Cool';

        $category = Category::where('name', 'Computers')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/cool.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Crock pot';
        $item->price = '40.00';
        $item->description = 'Beautiful Crock pot';

        $category = Category::where('name', 'Home')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/crockpot.jpeg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Disney Land';
        $item->price = '200.00';
        $item->description = 'Beautiful Disney Land';

        $category = Category::where('name', 'Outdoors')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/disneyland.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Drift Bike';
        $item->price = '200.00';
        $item->description = 'Beautiful Drift Bike';

        $category = Category::where('name', 'Outdoors')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/drift-bike.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Drone';
        $item->price = '200.00';
        $item->description = 'Beautiful Drone';

        $category = Category::where('name', 'Electronics')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/drone.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Fire TV';
        $item->price = '80.00';
        $item->description = 'Beautiful Fire TV';

        $category = Category::where('name', 'Electronics')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/firetv.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Frozen';
        $item->price = '999999.00';
        $item->description = 'Beautiful Frozen';

        $category = Category::where('name', 'Outdoors')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/frozen.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Galaxy S7';
        $item->price = '800.00';
        $item->description = 'Beautiful Galaxy S7';

        $category = Category::where('name', 'Electronics')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/galaxy7.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Grill';
        $item->price = '399.00';
        $item->description = 'Beautiful Grill';

        $category = Category::where('name', 'Mobile')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/grill.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Hawaii';
        $item->price = '3000.00';
        $item->description = 'Beautiful Hawaii';

        $category = Category::where('name', 'Outdoors')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/hawaii.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Hover Board';
        $item->price = '300.00';
        $item->description = 'Beautiful Hover Board';

        $category = Category::where('name', 'Electronics')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/hoverboard.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'iPad';
        $item->price = '400.00';
        $item->description = 'Beautiful iPad';

        $category = Category::where('name', 'Mobile')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/ipad.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'iPhone7';
        $item->price = '800.00';
        $item->description = 'Beautiful iPhone7';

        $category = Category::where('name', 'Mobile')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/iphone7.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Asus Laptop';
        $item->price = '600.00';
        $item->description = 'Beautiful Asus Laptop';

        $category = Category::where('name', 'Computers')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/laptop.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Macbook';
        $item->price = '1500.00';
        $item->description = 'Beautiful Macbook';

        $category = Category::where('name', 'Computers')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/macbook.jpeg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Michael Kors Handback';
        $item->price = '400.00';
        $item->description = 'Beautiful Michael Kors Handback';

        $category = Category::where('name', 'Apparel')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/mk.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Nike shoes';
        $item->price = '150.00';
        $item->description = 'Beautiful Nike Shoes';

        $category = Category::where('name', 'Shoes')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/nike.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Nikon Camera';
        $item->price = '350.00';
        $item->description = 'Beautiful Nikon Camera';

        $category = Category::where('name', 'Cameras')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/nikon.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Overwatch Digital Copy';
        $item->price = '350.00';
        $item->description = 'Beautiful Overwatch Digital Copy';

        $category = Category::where('name', 'Video Games')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/overwatch.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Scooter';
        $item->price = '750.00';
        $item->description = 'Beautiful Scooter';

        $category = Category::where('name', 'Motorcycles')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/scooter.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Segway';
        $item->price = '5000.00';
        $item->description = 'Beautiful Segway';

        $category = Category::where('name', 'Vehicles')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/segway.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Tie';
        $item->price = '50.00';
        $item->description = 'Beautiful Handmade Tie';

        $category = Category::where('name', 'Apparel')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/tie.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Tri Square';
        $item->price = '9999999.00';
        $item->description = 'Beautiful Tri Square';

        $category = Category::where('name', 'Computers')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/try_square.png';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Vegas';
        $item->price = '500.00';
        $item->description = 'Beautiful Vegas';

        $category = Category::where('name', 'Outdoors')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/vegas.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();

        $item = new Item;
        $item->title = 'Wallet';
        $item->price = '100.00';
        $item->description = 'Beautiful Wallet';

        $category = Category::where('name', 'Apparel')->first();
        $itemType = ItemType::where('type', 'joyit')->first();
        $item->category()->associate($category);
        $item->itemType()->associate($itemType);

        $item->save();

        $image = new Image;
        $image->path = '/img/products/wallet.jpg';
        $image->item()->associate($item);
        $imageType = ImageType::where('type', 'main')->first();
        $imageRatio = ImageRatio::where('ratio', '1:1')->first();
        $image->imageType()->associate($imageType);
        $image->imageRatio()->associate($imageRatio);
        $image->save();
    }
}
