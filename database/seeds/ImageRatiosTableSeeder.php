<?php

use Illuminate\Database\Seeder;
use App\ImageRatio;

class ImageRatiosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $itemType = new ImageRatio;
        $itemType->ratio = '1:1';
        $itemType->save();

        $itemType = new ImageRatio;
        $itemType->ratio = '1:2';
        $itemType->save();

        $itemType = new ImageRatio;
        $itemType->ratio = '2:1';
        $itemType->save();
    }
}
