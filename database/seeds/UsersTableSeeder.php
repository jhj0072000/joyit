<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 10; $i++)
        {
        	$thisUser = new User;
        	$thisUser->name = 'test'.$i;
        	$thisUser->email = 'test'.$i.'@test.com';
        	$thisUser->password = bcrypt('test123');
        	$thisUser->save();
        }
    }
}
