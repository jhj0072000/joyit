<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/user', function (Request $request) {
//     return $request->user();
// })->middleware('auth:api');


Route::resource('category', 'CategoryController', ['only' => [
    'index', 'store', 'show', 'update', 'destroy'
]]);

Route::resource('image', 'ImageController', ['only' => [
    'index', 'store', 'show', 'update', 'destroy'
]]);

Route::resource('item', 'ItemController', ['only' => [
    'index', 'store', 'show', 'update', 'destroy'
]]);

Route::resource('ticket', 'TicketController', ['only' => [
    'index', 'store', 'show', 'update', 'destroy'
]]);

Route::resource('user', 'UserController', ['only' => [
    'index', 'show', 'update', 'destroy'
]]);