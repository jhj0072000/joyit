<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group(['middleware' => 'auth'], function () {
	Route::get('/item/{item_id}', 'ItemController@show');
	Route::get('/myaccount', 'UserController@showMyAccount');
	Route::post('/joyit/{item_id}', 'TicketController@joyit');
	Route::post('/myaccount', 'UserController@update');
	Route::get('/item/winner/{item_id}', 'ItemController@selectWinner');
});


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');


// Route::get('/', function() {
// 	return view('home');
// });

Route::get('/new', function() {
	return view('new');
});

Route::get('/top50', 'ItemController@showTop50');

Route::get('/categories', function() {
	return view('categories');
});

Route::get('/categories', function() {
	return view('categories');
});

Route::get('/item', function() {
	return view('item');
});