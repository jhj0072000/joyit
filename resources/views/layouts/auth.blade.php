<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/custom.css" rel="stylesheet">
    <link href="/css/btn.css" rel="stylesheet">
    <link href="/css/presets.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <script src="/js/app.js"></script>
    <script>
        $(window).on('load', function() {
            setTimeout(function() {
                $(".loading").fadeOut("slow");;
            }, 500);
        });
    </script>
</head>
<body>
    <div id="app">
        @yield('content')
    </div>

    <div class="loading"></div>
    <script src="https://use.fontawesome.com/9cdd935747.js"></script>
</body>


</html>
