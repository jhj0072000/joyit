<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/custom.css" rel="stylesheet">
    <link href="/css/btn.css" rel="stylesheet">
    <link href="/css/presets.css" rel="stylesheet">
<!--     <link rel="stylesheet" href="http://getbootstrap.com.vn/examples/equal-height-columns/equal-height-columns.css" />
 -->
    <!-- Slick Slider -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <script src="/js/app.js"></script>
    
    <!-- Slick JS -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
   
    <script>
        $(window).on('load', function() {
            setTimeout(function() {
                $(".loading").hide();
            }, 500);
        });
    </script>
    <script>
        // if ( window.addEventListener ) {
        //     var keys = [], 
        //     troll = "84,82,79,76,76";
        //     window.addEventListener("keydown", function(e){
        //         keys.push( e.keyCode );
        //         if ( keys.toString().indexOf( troll ) >= 0 ) {
        //             $('#troll').modal();
        //         }
        //     }, true);
        // }
    </script>
</head>
<body style="z-index:1">
    <div class="loading"></div>
    <div id="app">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/home') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="/new">New</a>
                        </li>
                        <li>
                            <a href="/top50">Top 50</a>
                        </li>
                        <li class="dropdown" id="cate">
                            <a href="/categories" class="dropdown-toggle" data-toggle="dropdown">
                                Categories<i class="fa fa-angle-down fa-fw" aria-hidden="true"></i>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="#">
                                            Gifts<i class="fa fa-angle-right fa-fw pull-right" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Electronics<i class="fa fa-angle-right fa-fw pull-right" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Apparel<i class="fa fa-angle-right fa-fw pull-right" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Automobile<i class="fa fa-angle-right fa-fw pull-right" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Gadgets<i class="fa fa-angle-right fa-fw pull-right" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Books<i class="fa fa-angle-right fa-fw pull-right" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Toys & Games<i class="fa fa-angle-right fa-fw pull-right" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Cate 1
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Cate 2
                                        </a>
                                    </li><li>
                                        <a href="#">
                                            Cate 3
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Cate 4
                                        </a>
                                    </li>
                                </ul>
                            </a>
                        </li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Sign in</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/myaccount') }}">
                                            My Account
                                        </a>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Sign out
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                        <li class="dropdown" id="help">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Help<i class="fa fa-angle-down fa-fw" aria-hidden="true"></i>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Lol</a></li>
                                </ul>
                            </a>
                        </li>
                        <li><input type="text" class="form-control joyit-search" placeholder="Search joyit"></li>
                        <li><a href="#"><i class="fa fa-shopping-cart" aria-hidden="true"></i>
</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')

    </div>

    <div class="container-fluid villain">
        <h2>Win your life with our news letter.</h2>
        <div class="form-group">
        <input type="email" name="email" placeholder="Email Address" class="form-control">
        <button class="btn btn-default">Join</button>
        </div>
        
    </div>

    <footer>
        <hr>
        <div class="container">
            <div class="row" style="display:inherit">
                <div class="col-lg-12" style="display:inherit">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 terms-text">
                        <a href="#">Terms of Service</a> | <a href="#">Privacy</a>    
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 reserved-text">
                        <p class="muted">© 2016 Joyit. All rights reserved</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- <input type="text" name="fname" id="txtInput" autofocus style="position:absolute;z-index: 0;"> -->
    </footer>

    <!-- Scripts -->
    <!-- <script src="{{ asset('/js/vendor.js') }}"></script> -->
    <script src="https://unpkg.com/masonry-layout@4.1/dist/masonry.pkgd.min.js"></script>
    <script>
        // Initialize Grid System
        $(window).on('load', function() {
            $('.grid').masonry({
              itemSelector: '.grid-item',
              gutter: 5,
              columnWidth: 234,
              fitWidth: true
            });
        });
        // Navigation hover menus
        $('#cate, #help').hover(
            function() {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
            },
            function() {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
            }
        );
    </script>
    <script src="https://use.fontawesome.com/9cdd935747.js"></script>
</body>


</html>
