@extends('layouts.app')
@section('content')
<div class="container-fluid" style="margin-top:50px;">
	<div class="grid">
		<div class="grid-item grid-2">
			<a href="#" class="winit-box">
				<img src="https://placehold.it/473x473" style="width:100%">
				<div class="top-text-block">
                    <div class="top-text-block-inner">
                        check out our<br>hottest winit
                    </div>
                </div>
			</a>
		</div>
		<div class="grid-item">
			<a href="#" class="winit-box">
				<img src="https://placehold.it/234x234" style="width:100%">
	                <div class="block-layer-inner">
	                </div>
	            <div class="bottom-text-block-new">
                	<div class="top-box">1</div>
                	<div class="block-detail">
                		<div class="block-name">Text</div>
                        <div class="block-price">Price</div>
                	</div>
                </div>
	        </a>
		</div>
	</div>
</div>
@endsection
