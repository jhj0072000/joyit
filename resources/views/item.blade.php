@extends('layouts.app')

@section('content')

<div class="container-fluid product-topbar">
	<h4 class="text-center">
		<span style="color:red">Christmas</span> 
		<span style="color:green">Joyit</span> 
		is here! <a href="#">Shop Now</a>
	</h4>
</div>

<div class="container" style="margin-top:15px;padding:0px;">
	<div class="row tri-tags">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<p class="text-center"><b>{{$thisItem->getFilledPercentage()}}%</b> Full</p>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<p class="text-center"><b>{{$thisItem->price - $thisItem->tickets_count}}</b> slots remaining</p>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<p class="text-center"><b>Free</b> Shipping</p>
		</div>
	</div>
</div>
@if (\Session::has('message'))
   <div class="alert alert-info">{{ \Session::get('message') }}</div>
@endif
<div class="container main-item-box">
	<div class="row row-eq-height" style="background:white;">
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-left:0px;padding-right:0px;">
			<div class="product-slider">
                @foreach ($thisItem->images as $image)
    				<div class="product-single">
    					<img src="{{$image->path}}" style="width:100%">
    				</div>
                @endforeach 
			</div>
                
			<div class="thumb-nav">
                @foreach ($thisItem->images as $image)
                    <div class="product-single-thumbnail">
                        <img src="{{$image->path}}" style="width:100%">
                    </div>
                @endforeach
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="joyit-title-box">
				<h3 class="joyit-header">{{$thisItem->title}}</h3>
				<p>{{$thisItem->description}}</p>
			</div>
			<div class="joyit-price-box">
				<p style="margin-bottom:0px;"><b>Status:</b></p>
                @if($thisItem->tickets_count == $thisItem->price)
                    <span class="joyit-price">Sorry, this item is full already. Please select another item</span><br>
                    </div>
                @else
				    <span class="joyit-price">{{$thisItem->tickets_count}}/{{$thisItem->price}}</span><br>
                    </div>
                    <div class="joyit-cta-box">
                        <p><b>My Tickets:</b> <span id="joyit-ticket-amt">{{$user->getUnusedTicketCount()}}</span></p>
                        <p class="no-ticket" style="display:none">
                            * You have not enough minerals. 
                            <a href="#">Shop</a> to earn.
                        </p>
                        <button class="btn joyit-btn" id="joyit-now">JOYIT NOW</button>
                        <button class="btn wish-btn"><i class="fa fa-heart" style="color:#C0392B" aria-hidden="true"></i> Add to Wishlist</button>
                    </div>
                @endif
			
			<div class="joyit-list-box">
				<ul class="fa-ul">
					<li><i class="fa-li fa fa-check-square"></i>
						Lorem ipsum dolor sit amet, ad usu adhuc tamquam,ad usu adhuc tamquam,
					</li>
					<li><i class="fa-li fa fa-check-square"></i>
						Lorem ipsum dolor sit amet, ad usu adhuc tamquam,
					</li>
					<li><i class="fa-li fa fa-check-square"></i>
						Lorem ipsum dolor sit amet, ad usu adhuc tamquam,
					</li>
					<li><i class="fa-li fa fa-check-square"></i>
						Lorem ipsum dolor sit amet, ad usu adhuc tamquam,
					</li>
					<li><i class="fa-li fa fa-check-square"></i>
						Lorem ipsum dolor sit amet, ad usu adhuc tamquam,
					</li>
				</ul>
			</div>
			<div class="social-shares-box">
				<h4 style="margin-bottom:5px">3435 Shares</h4>
				<p>Click on icon below to share:</p>
				<ul class="list-inline">
					<li>
						<a href="#" class="social-icons">
							<i class="fa fa-facebook fa-2x" aria-hidden="true"></i>
						</a>
					</li>
					<li>
						<a href="#" class="social-icons">
							<i class="fa fa-twitter fa-2x" aria-hidden="true"></i>
						</a>
					</li>
					<li>
						<a href="#" class="social-icons">
							<i class="fa fa-envelope-o fa-2x" aria-hidden="true"></i>
						</a>
					</li>
				</ul>
			</div>
			<div class="item-page-ad-box">
				<img src="https://placehold.it/585x162?text=ads" style="width:100%">
			</div>
		</div>
	</div>
</div>

<div class="container main-item-detail-box" style="margin-top:15px;">
	<div class="row row-eq-height">
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 description-box">
			<h3>Description</h3>
			<p>Lorem ipsum dolor sit amet, te duo duis dolore. Dicat sapientem qui ne, wisi choro exerci vis cu, agam natum cu cum. Labores graecis expetendis ex per, te quo voluptaria consequuntur. Nam probo falli no, vix ut duis erant soleat. Id pri malorum iracundia, nec ne laoreet antiopam. Eum an placerat suavitate.</p>
			<p>Lorem ipsum dolor sit amet, te duo duis dolore. Dicat sapientem qui ne, wisi choro exerci vis cu, agam natum cu cum. Labores graecis expetendis ex per, te quo voluptaria consequuntur. Nam probo falli no, vix ut duis erant soleat. Id pri malorum iracundia, nec ne laoreet antiopam. Eum an placerat suavitate.</p>
			<h3>More Info</h3>
			<h5>Product Features:</h5>
			<ul class="list-circle">
				<li>lorem ipsum</li>
				<li>lorem ipsum</li>
				<li>lorem ipsum</li>
				<li>lorem ipsum</li>
				<li>lorem ipsum</li>
				<li>lorem ipsum</li>
			</ul>
			<h5>Dimentions:</h5>
			<ul class="list-circle">
				<li>100x100</li>
			</ul>
			<h5>Please Note:</h5>
			<ul class="list-circle">
				<li>lorem ipsum</li>
				<li>lorem ipsum</li>
				<li>lorem ipsum</li>
				<li>lorem ipsum</li>
				<li>lorem ipsum</li>
				<li>lorem ipsum</li>
			</ul>
			<p>Lorem ipsum dolor sit amet, te duo duis dolore. Dicat sapientem qui ne, wisi choro exerci vis cu, agam natum cu cum. Labores graecis expetendis ex per, te quo voluptaria consequuntur. Nam probo falli no, vix ut duis erant soleat. Id pri malorum iracundia, nec ne laoreet antiopam. Eum an placerat suavitate.</p>
			<p>Id sed eruditi insolens, ei timeam partiendo scriptorem eum, nominati consequuntur ne mel. Meis iriure sea an, ferri delectus evertitur vis no. In vivendum gubergren cum. Eros modus blandit eu usu. Antiopam torquatos sed ut.</p>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 related-box">
			<h3>Related Products</h3>
			<div class="grid-related">
                @foreach($relatedItems as $item)
    				<div class="grid-related-item">
                        <a href="/item/{{$item->id}}" class="joyit-box">
                            <img src="{{$item->images->first()->path}}" style="width:100%">
                            <div class="block-layer-inner">
                            </div>
                            <div class="bottom-text-block">
                                <div class="block-name">
                                    {{$item->title}}
                                </div>
                                <div class="block-price">
                                    {{$item->tickets_count}}/{{$item->price}}
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
			</div>
			<div class="slick-related">
				<div class="related-item">
					<img src="https://placehold.it/200x200" style="width:90%">
				</div>
				<div class="related-item">
					<img src="https://placehold.it/200x200" style="width:90%">
				</div>
				<div class="related-item">
					<img src="https://placehold.it/200x200" style="width:90%">
				</div>
				<div class="related-item">
					<img src="https://placehold.it/200x200" style="width:90%">
				</div>
				<div class="related-item">
					<img src="https://placehold.it/200x200" style="width:90%">
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container customer-review-box" style="background:white;margin-top:15px;">
	<h2 class="text-center">Customer Reviews</h2>
	<div class="customer-review">
		<div class="review-slide">
			<div class="rating">
				<span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
			</div>
			<div class="rating-user-box">
				<p>"A Great Laptop"</p>
				<p><i>Amazon User</i></p>
			</div>
			<div class="rating-pagination-box">
				<span><i>1 of 50 Reviews</i></span>
			</div>
		</div>
		<div class="review-slide">
			<div class="rating">
				<span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
			</div>
			<div class="rating-user-box">
				<p>"잇 싹!"</p>
				<p><i>Jeff</i></p>
			</div>
			<div class="rating-pagination-box">
				<span><i>2 of 50 Reviews</i></span>
			</div>
		</div>
	</div>
</div>
<div class="music"></div>
<!-- Modal -->
<div class="modal fade" id="joyitModal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<!-- <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Modal Title</h4>
			</div> -->
			<div class="modal-body">
				<img src="{{$thisItem->images->first()->path}}" style="width:100%">
				<p>You are bidding on <b>{{$thisItem->title}}</b></p>
				<p>Continue?</p>
			</div>
            <form id="joyit-form" action="/joyit/{{$thisItem->id}}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="ticket_count" value="1">
            </form>
            <div class="modal-footer">
                <button type="button" id="joyit-submit" class="btn btn-success">Yes</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
            </div>
			
		</div>
	</div>
</div>

<script>

	$('.product-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.thumb-nav'
	});
	$('.thumb-nav').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		asNavFor: '.product-slider',
		dots: false,
		centerMode: true,
		focusOnSelect: true,
		arrows: false,
		infinite: false,
	});
	$('.customer-review').slick({
		nextArrow: "<i class='fa fa-angle-right arrow-next' aria-hidden='true'></i>",
		prevArrow: "<i class='fa fa-angle-left arrow-prev' aria-hidden='true'></i>",
	});
	$('.slick-related').slick({
		infinite: false,
		autoplay: true,
		autoplaySpeed: 3000,
		slidesToShow: 3,
		slidesToScroll: 1
	});
	// Initialize Grid System
    $(window).on('load', function() {
        $('.grid-related').masonry({
          itemSelector: '.grid-related-item',
          gutter: 15,
          columnWidth: 175,
          fitWidth: true
        });
    });

	$('.spinner .btn:first-of-type').on('click', function() {
		$('.spinner input').val( parseInt($('.spinner input').val(), 10) + 1);
	});

	$('.spinner .btn:last-of-type').on('click', function() {
		var input = $('.spinner input').val();
		if(input === 0) {
			console.log(input);
		} else {
			$('.spinner input').val( parseInt($('.spinner input').val(), 10) - 1);
		}
	});

	$('#joyit-now').on('click', function() {
		console.log($('#joyit-ticket-amt').text());
		if($('#joyit-ticket-amt').text() === "0") {
			$('.no-ticket').show(300);
			$('.music').append('<audio src="/img/pylon.mp3" autoplay></audio>');
		} else {
			$('#joyitModal').modal('show');
		}
	});

    $('#joyit-submit').on('click', function() {
        $('#joyit-form').submit();
    });

</script>










@endsection