@extends('layouts.app')

@section('content')


<div class="container" style="margin-top:100px;">
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
	<div class="row">
		<div class="col-lg-2">
			<div class="profile-picture">
				<img src="https://placehold.it/300x300" style="width:100%">
			</div>
			<div class="profile-summary">
				<p>{{$user->name}}</p>
			</div>
		</div>
		<div class="col-lg-10">
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#home">Account</a></li>
				<li><a data-toggle="tab" href="#menu1">Joyit</a></li>
				<li><a data-toggle="tab" href="#menu2">Wishlist</a></li>
			</ul>
			<div class="tab-content">
				<div id="home" class="tab-pane fade in active">
					<h3>Account</h3>
					<form action="/myaccount" method="POST" id="account-form" class="form-horizontal">
						{{ csrf_field() }}
						<div class="form-group">
							<label class="col-lg-1 control-label">Name:</label>
							<div class="col-lg-8">
								<input name="fullName" class="form-control" value="{{$user->name}}" type="text">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-1 control-label">Email:</label>
							<div class="col-lg-8">
								{{$user->email}}
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-1 control-label">Joined:</label>
							<div class="col-lg-8">
								<p class="account-info-space">{{$user->created_at}}</p>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-1 control-label">Address:</label>
							<div class="col-lg-8">
								@if($user->addressInfos->count() == 0)
									<input name="address1" class="form-control" placeholder="Address Line 1" value="" type="text">
									<input name="address2" class="form-control" placeholder="Address Line 2 (Optional)" value="" type="text">
									<input name="city" class="form-control" placeholder="City" value="" type="text">
									<input name="state" class="form-control" placeholder="State" value="" type="text">
									<input name="zipcode" class="form-control" placeholder="Zip Code" value="" type="text">
								@else
									<input name="address1" class="form-control" value="{{$user->addressInfos->first()->address1}}" type="text">
									<input name="address2" class="form-control" value="{{$user->addressInfos->first()->address2}}" type="text">
									<input name="city" class="form-control" value="{{$user->addressInfos->first()->city}}" type="text">
									<input name="state" class="form-control" value="{{$user->addressInfos->first()->state}}" type="text">
									<input name="zipcode" class="form-control" value="{{$user->addressInfos->first()->zipcode}}" type="text">
								@endif
							</div>
						</div>
					</form>
					<button type="submit" form="account-form" value="Submit">Submit</button>
				</div>
				<div id="menu1" class="tab-pane fade">
					<h3>Joyit</h3>
					<table class="table">
						<thead>
							<tr>You have <span>{{$items->count()}}</span> entries</tr>
						</thead>
						<tbody>
							@foreach($items as $item)
								<tr>
									<td>{{number_format($item->tickets->count()/$item->price, 2, '.', '')}}%</td>
									<td><img src="{{$item->images->first()->path}}" style="width:134px"></td>
									<td>{{$item->title}}</td>
									@if($item->tickets->count() != $item->price)
										<td>This item is not full yet</td>
									@elseif($item->tickets->where('user_id', $user->id)->where('won', 1)->count() > 0)
										<td>Congratulation You have won this product</td>
									@else
										<td>Sorry, another user won this product</td>
									@endif
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div id="menu2" class="tab-pane fade">
					<h3>Wishlist</h3>
					<table class="table table-responsive">
						<thead>
							<tr>You have <span>5</span> entries</tr>
						</thead>
						<tbody>
							<tr>
								<td class="col-lg-1">100% Full</td>
								<td class="col-lg-2"><img src="https://placehold.it/70x70" style="width:100%"></td>
								<td class="col-lg-6">Entered, price</td>
								<td class="col-lg-3"><button class="btn joyit-btn">JOYIT</button></td>
							</tr>
							<tr>
								<td class="col-lg-1">100% Full</td>
								<td class="col-lg-2"><img src="https://placehold.it/70x70" style="width:100%"></td>
								<td class="col-lg-6">Entered, price</td>
								<td class="col-lg-3"><button class="btn joyit-btn">JOYIT</button></td>
							</tr>
							<tr>
								<td class="col-lg-1">100% Full</td>
								<td class="col-lg-2"><img src="https://placehold.it/70x70" style="width:100%"></td>
								<td class="col-lg-6">Entered, price</td>
								<td class="col-lg-3"><button class="btn joyit-btn">JOYIT</button></td>
							</tr>
							<tr>
								<td class="col-lg-1">100% Full</td>
								<td class="col-lg-2"><img src="https://placehold.it/70x70" style="width:100%"></td>
								<td class="col-lg-6">Entered, price</td>
								<td class="col-lg-3"><button class="btn joyit-btn">JOYIT</button></td>
							</tr>
							<tr>
								<td class="col-lg-1">100% Full</td>
								<td class="col-lg-2"><img src="https://placehold.it/70x70" style="width:100%"></td>
								<td class="col-lg-6">Entered, price</td>
								<td class="col-lg-3"><button class="btn joyit-btn">JOYIT</button></td>
							</tr>
							<tr>
								<td class="col-lg-1">100% Full</td>
								<td class="col-lg-2"><img src="https://placehold.it/70x70" style="width:100%"></td>
								<td class="col-lg-6">Entered, price</td>
								<td class="col-lg-3"><button class="btn joyit-btn">JOYIT</button></td>
							</tr>
							<tr>
								<td class="col-lg-1">100% Full</td>
								<td class="col-lg-2"><img src="https://placehold.it/70x70" style="width:100%"></td>
								<td class="col-lg-6">Entered, price</td>
								<td class="col-lg-3"><button class="btn joyit-btn">JOYIT</button></td>
							</tr>
							<tr>
								<td class="col-lg-1">100% Full</td>
								<td class="col-lg-2"><img src="https://placehold.it/70x70" style="width:100%"></td>
								<td class="col-lg-6">Entered, price</td>
								<td class="col-lg-3"><button class="btn joyit-btn">JOYIT</button></td>
							</tr>
							<tr>
								<td class="col-lg-1">100% Full</td>
								<td class="col-lg-2"><img src="https://placehold.it/70x70" style="width:100%"></td>
								<td class="col-lg-6">Entered, price</td>
								<td class="col-lg-3"><button class="btn joyit-btn">JOYIT</button></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</div>

@endsection

