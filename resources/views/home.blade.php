@extends('layouts.app')

@section('content')
<!-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- <div class="modal fade" id="troll" tabindex="-1" role="dialog" style="display:flex;align-items:center;justify-content:center;">
  <img src="http://vignette2.wikia.nocookie.net/roblox/images/3/38/Transparent_Troll_Face.png/revision/latest?cb=20120713214853" style="position:absolute;width:70%">
</div> -->
<div class="container-fluid page-heading text-center" style="margin-top:50px;">
    <h1 class="text-center home-banner-text" style="font-weight:bold;">
        <span style="color:#e74c3c">Need</span> <span style="color:#16a085">Joyit</span> points?
    </h1>
    <button class="btn btn-default main-btn">SHOP NOW</button>
</div>


<div class="container-fluid home-joyit-wrapper">
    <div class="grid">
        @foreach($items as $item)
        
            @if($loop->first || $loop->index == 1)
                <div class="grid-item grid-2">
                    <a href="/item/{{$item->id}}" class="joyit-box">
                        <img src="{{$item->images->first()->path}}" style="width:100%">
                        <div class="top-text-block">
                            <div class="top-text-block-inner">
                                check out our<br>hottest joyit
                            </div>
                        </div>
                        
            
            @else
                <div class="grid-item">
                    <a href="/item/{{$item->id}}" class="joyit-box">
                        <img src="{{$item->images->first()->path}}" style="width:100%">
                            <div class="block-layer-inner">
                            </div>
                        
            @endif
                        <div class="bottom-text-block">
                            <div class="block-name">
                                {{$item->title}}
                            </div>
                            <div class="block-price">
                                {{$item->tickets_count}} / {{$item->price}}
                            </div>
                        </div>
                    </a>
                </div>
        @endforeach
    </div>
</div>
@endsection










