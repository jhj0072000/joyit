import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class App extends Component {
    constructor(props) {
        super();
    }

    render() {
        return (<div>Hello World!</div>);
    }
}

ReactDOM.render(<App />, document.querySelector('#app'));